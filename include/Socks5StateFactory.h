#ifndef __Socks5StateFactory_H_
#define __Socks5StateFactory_H_

#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>

#include <Connection.h>
#include <Socks5State.h>

/*
 * struct definition of class and external definition of class table
 */
struct Socks5StateFactory_class{
	void* super;
	struct Socks5State* (*make)(void*);
};
extern struct Socks5StateFactory_class Socks5StateFactory_class_table;

/*
 * struct definition of object
 */
struct Socks5StateFactory{
	struct TSocks_class* _class;
	// interface have no instant variable
};


/*
 * definition of methods implemented by class
 */
struct Socks5State* make(void*);
#endif
