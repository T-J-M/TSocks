#ifndef __ListenConnFactory_H_
#define __ListenConnFactory_H_

#include <Connection.h>
#include <ConnectionFactory.h>
#include <stdbool.h>

#define ENABLE_REUSEADDR true

/*
 * struct definition of class and external definition of class table
 */
struct ListenConnFactory_class {
	void* super;
	//static method
	struct Connection* (*make)(void*);
};
extern struct ListenConnFactory_class ListenConnFactory_class_table;

//Singleton
struct Connection* make(void*);
#endif
