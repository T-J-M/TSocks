#ifndef __ExistConnState_H_
#define __ExistConnState_H_

#include "ConnectionState.h"

/*
 * struct definition of class and external definition of class table
 */
//Interface
struct ExistConnState_class{
	void* super;
	int (*handle)(void*);
};
extern struct ExistConnState_class ExistConnState_class_table;

/*
 * struct definition of object
 */
int handle(void*);
#endif
