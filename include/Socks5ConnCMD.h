#ifndef __Socks5ConnCMD_H_
#define __Socks5ConnCMD_H_

#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>

#include <Connection.h>
#include <Socks5State.h>

/*
 * struct definition of class and external definition of class table
 */
struct Socks5ConnCMD_class {
	void* super;
	int (*handle)(void*, struct Connection*);
	struct Socks5State* (*next)(void*);
	void* (*destruct)(void*);
};

extern struct Socks5ConnCMD_class Socks5ConnCMD_class_table;

/*
 * struct definition of object
 */
struct Socks5ConnCMD{
	struct Socks5ConnCMD_class* _class;
	
	// instance variables defined in this class
	struct Socks5State* next;
	struct sockaddr_in* dst_addr;
};

/*
 * definition of methods implemented by class
 */
void Socks5ConnCMD_ctor(void*,void*);
int handle(void*,struct Connection*);
struct Socks5State* next(void*);
void* destruct(void*);
#endif
