#ifndef __ConnectionListener_H_
#define __ConnectionListener_H_

#include <sys/epoll.h>

#ifndef EPOLL_QUEUE_LEN
#define EPOLL_QUEUE_LEN 4096
#endif

/*
 * struct definition of class and external definition of class table
 */
struct ConnectionListener_class{
	void* super;
	void (*addConn)(void*, void*, uint32_t);
	void (*removeConn)(void*, void*);
	void (*sleep)(void*);
	void* (*destruct)(void*);
};

extern struct ConnectionListener_class ConnectionListener_class_table;

/*
 * struct definition of object
 */
struct ConnectionListener;
struct ConnectionListener{
	struct ConnectionListener_class* _class;

	// instance variables defined in this class
	int epfd;
	struct epoll_event events[EPOLL_QUEUE_LEN];
};

/*
 * definition of methods implemented by class
 */
//Constructor, init with listening event
void ConnectionListener_ctor(void*);
void* destruct(void*);
void addConn(void*, void*, uint32_t);
void removeConn(void*, void*);
void sleep(void*);

#endif
