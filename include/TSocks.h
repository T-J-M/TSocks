#ifndef __TSocks_H_
#define __TSocks_H_

#include <sys/types.h>
#include <netinet/in.h>
#include <TSocksProperties.h>

/*
 * struct definition of class and external definition of class table
 */
struct TSocks_class{
	void* super;
	int (*run)(void*);
	struct TSocksProperties* (*getProperties)(void*);
	void (*setProperties)(void*,struct TSocksProperties*);
	void* (*destruct)(void*);
};

extern struct TSocks_class TSocks_class_table;

/*
 * struct definition of object
 */
struct TSocks{
	struct TSocks_class* _class;

	// instance variables defined in this class
	struct TSocksProperties* properties;
};

/*
 * definition of methods implemented by class
 */
void TSocks_ctor(void*);
int run(void*);
struct TSocksProperties* getProperties(void*);
void setProperties(void*, struct TSocksProperties*);
void* destruct(void*);
#endif
