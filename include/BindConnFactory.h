#ifndef __BindConnFactory_H_
#define __BindConnFactory_H_

#include <Connection.h>
#include <ConnectionFactory.h>

/*
 * struct definition of class and external definition of class table
 */
struct BindConnFactory_class {
	void* super;
	//static method
	struct Connection* (*make)(void*);
};
extern struct BindConnFactory_class BindConnFactory_class_table;

/*
 * struct definition of object
 */
struct Connection* make(void*);

#endif
