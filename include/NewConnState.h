#ifndef __NewConnState_H_
#define __NewConnState_H_

#include "ConnectionState.h"

/*
 * struct definition of class and external definition of class table
 */
struct NewConnState_class{
	void* super;
	int (*handle)(void*);
};
extern struct NewConnState_class NewConnState_class_table;

/*
 * struct definition of object
 */
int handle(void*);
#endif
