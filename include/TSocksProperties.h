#ifndef __TSocksProperties_H_
#define __TSocksProperties_H_

#include <sys/types.h>
#include <netinet/in.h>

/*
 * struct definition of class and external definition of class table
 */
struct TSocksProperties_class{
	void *super;
	void (*setHost)(void*, char*);
	char* (*getHostStr)(void*);
	struct in_addr (*getHost)(void*);
	void (*setPort)(void*, unsigned short);
	unsigned short (*getPort)(void*);
	void (*setListen)(void*, unsigned short);
	unsigned short (*getListen)(void*);
	void (*setListenQ)(void*, unsigned short);
	unsigned short (*getListenQ)(void*);
	void* (*destruct)(void*);
};
extern struct TSocksProperties_class TSocksProperties_class_table;

/*
 * struct definition of object
 */
struct TSocksProperties{
	struct TSocksProperties_class* _class;

	// instance variables defined in this class
	struct in_addr socks_host;
	unsigned short socks_port;
	unsigned short listen_port;
	int listenQ;
};

/*
 * definition of methods implemented by class
 */
void TSocksProperties_ctor(void*);
void setHost(void*, char*);	
char* getHostStr(void*);
struct in_addr getHost(void*);
void setPort(void*, unsigned short);
unsigned short getPort(void*);
void setListen(void*, unsigned short);
unsigned short getListen(void*);
void setListenQ(void*, unsigned short);
unsigned short getListenQ(void*);
void* destruct(void*);

#endif
