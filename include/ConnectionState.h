#ifndef __ConnectionState_H_
#define __ConnectionState_H_

#include "Connection.h"

/*
 * struct definition of class and external definition of class table
 */
//Interface
struct ConnectionState_class{
	void* super;
	int (*handle)(void*);
};
extern struct ConnectionState_class ConnectionState_class_table;

//Abstruct class no constructor
#endif
