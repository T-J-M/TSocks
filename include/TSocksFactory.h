#ifndef __TSocksFactory_H_
#define __TSocksFactory_H_

/*
 * struct definition of class and external definition of class table
 */
struct TSocksFactory_class{
	void *super;
	//static method
	struct TSocks* (*make)(void*);
};
extern struct TSocksFactory_class TSocksFactory_class_table;

//Singleton

struct TSocks* make(void*);
#endif
