#ifndef __TSocksPropertiesFactory_H_
#define __TSocksPropertiesFactory_H_

/*
 * struct definition of class and external definition of class table
 */
struct TSocksPropertiesFactory_class{
	void *super;
	//static method
	struct TSocksProperties* (*make)(void*);
};
extern struct TSocksPropertiesFactory_class TSocksPropertiesFactory_class_table;

//Singleton

struct TSocksProperties* make(void*);
#endif
