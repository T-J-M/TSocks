#ifndef __Connection_H_
#define __Connection_H_

#include "ConnectionState.h"

/*
 * struct definition of class and external definition of class table
 */
struct Connection_class{
	void* super;
	struct ConnectionState_class* (*getState)(void*);
	void (*setState)(void*, void*);
	struct Connection* (*getPeerConn)(void*);
	void (*setPeerConn)(void*, struct Connection*);
	int (*getFD)(void*);
	void (*setFD)(void*,int);
	void* (*destruct)(void*);
};

extern struct Connection_class Connection_class_table;

/*
 * struct definition of object
 */
struct Connection;
struct Connection{
	struct Connection_class* _class;

	// instance variables defined in this class
	struct ConnectionState* state;
	struct Connection* peer;
	int sockfd;
};

/*
 * definition of methods implemented by class
 */
void Connection_ctor(void*);
struct ConnectionState_class* getState(void*);
void setState(void*, void*);
struct Connection* getPeerConn(void*);
void setPeerConn(void*, struct Connection*);
int getFD(void*);
void setFD(void*,int);
void* destruct(void*);

#endif
