#ifndef __Socks5HandShake_H_
#define __Socks5HandShake_H_

#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>

#include <Connection.h>
#include <Socks5State.h>

/*
 * struct definition of class and external definition of class table
 */
struct Socks5HandShake_class{
	void* super;
	int (*handle)(void*,struct Connection*);
	struct Socks5State* (*next)(void*);
	void* (*destruct)(void*);
};

extern struct Socks5HandShake_class Socks5HandShake_class_table;

/*
 * struct definition of object
 */
struct Socks5HandShake{
	struct Socks5HandShake_class* _class;
	
	// instance variables defined in this class
	struct Socks5State* next;
};

/*
 * definition of methods implemented by class
 */
void Socks5HandShake_ctor(void*, void*);
int handle(void*,struct Connection*);
struct Socks5State* next(void*);
void* destruct(void*);
#endif
