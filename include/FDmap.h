#ifndef __FDmap_H_
#define __FDmap_H_

/*
 * struct definition of class and external definition of class table
 */
struct FDmap_class{
	void* super; 
	void (*insert)(void*,int,void*);
	void* (*query)(void*,int);
	void* (*destruct)(void*);
};
extern struct FDmap_class FDmap_class_table;

/*
 * struct definition of object
 */
struct FDmap{
	struct FDmap_class* _class;

	// instance variables defined in this class
	int size;
	void* data;
};

/*
 * definition of methods implemented by class
 */
void FDmap_ctor(void*);
void insert(void*,int,void*);
void* query(void*,int);
void* destruct(void*);

#endif
