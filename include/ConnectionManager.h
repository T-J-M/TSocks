#ifndef __ConnectionManager_H_
#define __ConnectionManager_H_

#include <FDmap.h>
#include <Connection.h>

/*
 * struct definition of class and external definition of class table
 */
struct ConnectionManager_class{
	void *super;
	//static method
	struct ConnectionManager* (*getInstance)();
	//object method
	struct Connection* (*getConnByFD)(void*,int);
	struct Connection* (*getNewConn)(void*,void*,void*);
	void (*closeConn)(void*,void*);
};
extern struct ConnectionManager_class ConnectionManager_class_table;

/*
 * struct definition of object
 */
struct ConnectionManager {
	struct ConnectionManager_class* _class;

	// instance variables defined in this class
	struct FDmap* map;
};

//Singleton
struct ConnectionManager* getInstance();
struct Connection* getConnByFD(void*,int);
struct Connection* getNewConn(void*,void*,void*);
void closeConn(void*,void*);

#endif
