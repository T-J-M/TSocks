#ifndef __Socks5State_H_
#define __Socks5State_H_

#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>

#include <Connection.h>

/*
 * struct definition of class and external definition of class table
 */
struct Socks5State_class{
	void* super;
	int (*handle)(void*,struct Connection*);
	struct Socks5State* (*next)(void*);
	void* (*destruct)(void*);
};

extern struct Socks5State_class Socks5State_class_table;

/*
 * struct definition of object
 */
struct Socks5State{
	struct Socks5State_class* _class;
	// interface have no instant variable
};


/*
 * definition of methods implemented by class
 */
int handle(void*,struct Connection*);
struct Socks5State* next(void*);
void* destruct(void*);
#endif
