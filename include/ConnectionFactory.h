#ifndef __ConnectionFactory_H_
#define __ConnectionFactory_H_

#include <Connection.h>

/*
 * struct definition of class and external definition of class table
 */
struct ConnectionFactory_class {
	void* super;
	//static method
	struct Connection* (*make)(void*);
};
extern struct ConnectionFactory_class ConnectionFactory_class_table;

/*
 * struct definition of object
 */
struct ConnectionFactory{
	struct ConnectionFactory_class* _class;
	//Abstract class as Interface
	//No instance 
};

/*
 * definition of methods implemented by class
 */
struct Connection* make(void*);
#endif
