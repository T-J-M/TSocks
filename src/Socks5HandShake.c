#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <Socks5State.h>
#include <Socks5HandShake.h>
#include <Socks5ConnCMD.h>
#include <refcount.h>

struct Socks5HandShake_class Socks5HandShake_class_table = {
	&Socks5State_class_table, /* super */
	handle,
	next
};

void Socks5HandShake_ctor(void* thisv, void* argv){
	struct Socks5HandShake* this = thisv;
	struct Socks5State* next_state = argv;
	bzero(this,sizeof(struct Socks5HandShake));
	this->_class = &Socks5HandShake_class_table;
	this->next = next_state;
	rc_keep_ref(next_state);
}

#define SOCKSVER 0x05
#define AUTH_NMETHODS 0x01
#define NO_AUTH_REQ 0x00

int handle(void* thisv, struct Connection* conn){
	struct Socks5HandShake * this = thisv;
	int fd = conn->_class->getFD(conn);

	//Handshake with socks5 server
	const char request[] = {SOCKSVER,AUTH_NMETHODS,NO_AUTH_REQ};
	char response[2]; memset(response,0,sizeof(response));
	write(fd,request,sizeof(request));
	read(fd,response,sizeof(response));

	//Version 5 and no auth required
	return (0x05!=response[0]||0x00!=response[1]);
}

struct Socks5State* next(void* thisv){
	struct Socks5HandShake* this = thisv;
	return this->next;
}

void* destruct(void* thisv){
	struct Socks5HandShake* this = thisv;
	rc_free_ref(this->next);
	rc_free_ref(this);
	return NULL;
}
