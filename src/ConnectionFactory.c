#include <ConnectionFactory.h>
#include <Connection.h>
#include <stdlib.h>
#include <string.h>
#include <refcount.h>

#include <netinet/in.h>
#include <sys/socket.h>

struct ConnectionFactory_class ConnectionFactory_class_table = {
	NULL, /* super */
	make
};


struct Connection* make(void* factory_argv){
	struct Connection * conn = rc_malloc(sizeof(struct Connection));
	bzero(conn,sizeof(struct Connection));
	Connection_ctor(conn);
	conn->_class->setPeerConn(conn,NULL);
	int fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	conn->_class->setFD(conn,fd);
	return conn;
}
