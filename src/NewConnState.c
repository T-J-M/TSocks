#include <ConnectionState.h>
#include <Connection.h>
#include <NewConnState.h>
#include <Socks5StateFactory.h>
#include <Socks5State.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <assert.h>
#include <linux/netfilter_ipv4.h>
#include <sys/epoll.h>

#define SA struct sockaddr

struct NewConnState_class NewConnState_class_table = {
	&ConnectionState_class_table, /* super */
	handle
};

#define SOCKSVER 0x05
#define AUTH_NMETHODS 0x01
#define NO_AUTH_REQ 0x00

#define CMD_CONN 0x01
#define ATYPE_INET4 0x01
#define ATYPE_INET6 0x04
int handle(void* thisv){
	struct Connection * conn = thisv;
	int listenfd = conn->_class->getFD(conn);
	// Accept new connection
	struct sockaddr_in cliaddr; bzero(&cliaddr,sizeof(cliaddr));
	socklen_t cliaddr_len = sizeof(cliaddr);
	int acceptfd = accept(listenfd,(SA *) &cliaddr,&cliaddr_len);
	if(-1==acceptfd){
		// TODO: error handling
	} else {
		struct sockaddr_in dstaddr; bzero(&dstaddr,sizeof(dstaddr));
		socklen_t dstaddr_len = sizeof(dstaddr);
		if(-1==getsockopt(acceptfd,SOL_IP,SO_ORIGINAL_DST,&dstaddr,&dstaddr_len)) {
			// TODO: error handling
			// Warning: socket fd should be closed
		} else {
			struct Socks5State* socks5state = Socks5StateFactory_class_table.make(&dstaddr);
			struct Socks5State* pastState = NULL;
			while(NULL!=socks5state->_class->next(socks5state)){
				if(NULL!=pastState){
					pastState = pastState->_class->destruct(pastState);
				}
				int result = socks5state->_class->handle(socks5state,conn);
				//TODO: check result value and handle error
				pastState = socks5state;
				socks5state = socks5state->_class->next(socks5state);
			}
			pastState = pastState->_class->destruct(pastState);
		}
	}
	return 0;
}
