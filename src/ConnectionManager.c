#include <ConnectionManager.h>
#include <ConnectionFactory.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

struct ConnectionManager_class ConnectionManager_class_table = {
	NULL, /* super */
	getInstance,
	getConnByFD,
	getNewConn,
	closeConn
};

struct ConnectionManager* instance = NULL;

struct ConnectionManager* getInstance(){
	if(NULL==instance){
		instance = malloc(sizeof(struct ConnectionManager));
		bzero(instance,sizeof(struct ConnectionManager));
		instance->_class = &ConnectionManager_class_table;
		instance->map = malloc(sizeof(struct FDmap));
		FDmap_ctor(instance->map);
	}
	return instance;
}

struct Connection* getConnByFD(void* thisv, int fd){
	struct ConnectionManager* this = thisv;
	struct FDmap* map = this->map;
	struct Connection* rst = map->_class->query(map,fd);
	return rst;
}

struct Connection* getNewConn(void* thisv, void* factoryv, void* factory_argv){
	struct ConnectionManager* this = thisv;
	struct FDmap* map = this->map;
	struct ConnectionFactory* factory = factoryv;
	struct Connection* conn = factory->_class->make(factory_argv);
	int fd = conn->_class->getFD(conn);
	map->_class->insert(map,fd,conn);
	return conn;
}

void do_conn_close(struct FDmap* map, struct Connection* conn){
	int fd = conn->_class->getFD(conn);
	close(fd);
	map->_class->insert(map,fd,NULL);
}

void closeConn(void* thisv,void* connv){
	struct ConnectionManager* this = thisv;
	struct Connection* conn = connv;
	struct FDmap* map = this->map;
	struct Connection* pair = conn->_class->getPeerConn(conn);
	do_conn_close(map,conn);
	do_conn_close(map,pair);
}
