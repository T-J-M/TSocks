#include <Socks5State.h>
#include <Socks5ConnCMD.h>
#include <refcount.h>

#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <assert.h>

struct Socks5ConnCMD_class Socks5ConnCMD_class_table = {
	&Socks5State_class_table, /* super */
	handle,
	next,
	destruct
};

struct Socks5ConnCMD instance = {
	&Socks5ConnCMD_class_table,
	NULL,
	NULL
};

void Socks5ConnCMD_ctor(void* thisv, void* argv){
	struct Socks5ConnCMD* this = thisv;
	bzero(this,sizeof(struct Socks5ConnCMD));
	this->_class = &Socks5ConnCMD_class_table;
	void** args = argv;
	struct Socks5State* next = args[0];
	struct sockaddr_in* dst_addr = args[1];
	this->next = next;
	rc_keep_ref(next);
	this->dst_addr = dst_addr;
	rc_keep_ref(dst_addr);
}

void* destruct(void* thisv){
	struct Socks5ConnCMD* this = thisv;
	rc_free_ref(this->next);
	rc_free_ref(this->dst_addr);
	rc_free_ref(this);
	return NULL;
}

#define SOCKSVER 0x05
#define CMD_CONN 0x01
#define ATYPE_INET4 0x01
#define ATYPE_INET6 0x04

int handle(void* thisv,struct Connection* conn){
	struct Socks5ConnCMD* this = thisv;
	struct sockaddr_in* dstaddr = this->dst_addr;
	int fd = conn->_class->getFD(conn);

	//Send remote connect request
	const char request_head[] = {SOCKSVER,CMD_CONN,0x00};
	int request_len = 10;
	char * request = malloc(request_len);  bzero(request,request_len);
	//Request head
	memcpy(request,request_head,sizeof(request_head));
	int offset = sizeof(request_head);
	//ATYPE
	request[offset++] = ATYPE_INET4;
	//DST.ADDR
	memcpy(request+offset,(char *)&dstaddr->sin_addr,sizeof(dstaddr->sin_addr));
	offset += sizeof(dstaddr->sin_addr);
	//DST.PORT
	memcpy(request+offset,(char *)&dstaddr->sin_port,sizeof(dstaddr->sin_port));
	offset += sizeof(dstaddr->sin_port);
	assert(request_len==offset);
	write(fd,request,request_len);
	free(request);
	//Get response
	int response_len = 10;
	char * response = malloc(response_len); bzero(response,response_len);
	read(fd,response,response_len);
	//Version 5 and success
	return (0x05!=response[0]||0x00!=response[1]);
}

struct Socks5State* next(void* thisv){
	struct Socks5ConnCMD* this = thisv;
	return this->next;
}
