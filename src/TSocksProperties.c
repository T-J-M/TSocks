#include <TSocksProperties.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stddef.h>
#include <string.h>
#include <refcount.h>

#define AF_FAMILY AF_INET
#define MAX_BUFFER_SIZE 65536

struct TSocksProperties_class TSocksProperties_class_table = {
	NULL, /* super */
	setHost,
	getHostStr,
	getHost,
	setPort,
	setListen,
	getListen,
	setListenQ,
	getListenQ,
	destruct
};

void* destruct(void* thisv){
	struct TSocksProperties* this = thisv;
	rc_free_ref(this);
	return NULL;
}

//Constructor
void TSocksProperties_ctor(void* thisv){
	struct TSocksProperties* this = thisv;
	bzero(this,sizeof(struct TSocksProperties));
	this->_class = &TSocksProperties_class_table;
}

void setHost(void* thisv, char* hostP){
	struct TSocksProperties* this = thisv;
	inet_pton(AF_FAMILY,hostP,&this->socks_host);
}

char* getHostStr(void* thisv){
	struct TSocksProperties* this = thisv;
	const int buffer_size = MAX_BUFFER_SIZE;
	char * str = rc_malloc(buffer_size);
	str = inet_ntop(AF_FAMILY,&this->socks_host,str,buffer_size);
	return str;
}

struct in_addr getHost(void* thisv){
	struct TSocksProperties* this = thisv;
	return this->socks_host;
}

void setPort(void* thisv, unsigned short port){
	struct TSocksProperties* this = thisv;
	this->socks_port = port;
}

unsigned short getPort(void* thisv){
	struct TSocksProperties* this = thisv;
	return this->socks_port;
}

void setListen(void* thisv, unsigned short listen){
	struct TSocksProperties* this = thisv;
	this->listen_port = listen;
}

unsigned short getListen(void* thisv){
	struct TSocksProperties* this = thisv;
	return this->listen_port;
}

void setListenQ(void* thisv, unsigned short listenQ){
	struct TSocksProperties* this = thisv;
	this->listenQ = listenQ;
}

unsigned short getListenQ(void* thisv){
	struct TSocksProperties* this = thisv;
	return this->listenQ;
}

