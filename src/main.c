#include <ctype.h>
#include <limits.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include <TSocks.h>
#include <TSocksFactory.h>
#include <TSocksProperties.h>
#include <TSocksPropertiesFactory.h>

#define DEFAULT_SOCKS_HOST "127.0.0.1"
#define DEFAULT_SOCKS_PORT 1081
#define DEFAULT_LISTEN_PORT 8086

#ifndef LISTENQ
#define LISTENQ 1024
#endif

void print_useage();
void print_help();
unsigned short itous(int);
unsigned short valid_port(unsigned short,char*,unsigned short);

int main(int argc, char* argv[]){
	char* socks_host = DEFAULT_SOCKS_HOST;
	unsigned short socks_port = DEFAULT_SOCKS_PORT;
	unsigned short listen_port = DEFAULT_LISTEN_PORT;

	//Parse option switch
	opterr = 0;
	const char* optstr = "s:p:l:h";
	char c;
	while((c=getopt(argc,argv,optstr))!=-1){
		switch(c){
			case 's':
				socks_host = optarg;
				break;
			case 'p':
				socks_port = itous(atoi(optarg));
				socks_port = valid_port(socks_port,"Socks5 Server Port",DEFAULT_SOCKS_PORT);
				break;
			case 'l':
				listen_port = itous(atoi(optarg));
				listen_port = valid_port(listen_port,"Listen Port",DEFAULT_LISTEN_PORT);
				break;
			case 'h':
				print_help();
				break;
			case '?':
				print_useage();
				break;
			default:
				exit(EXIT_FAILURE);

		}
	}

	//Confiture TSocks Instance
	struct TSocksProperties* property = TSocksPropertiesFactory_class_table.make(NULL);
	property->_class->setHost(property,socks_host);
	property->_class->setPort(property,socks_port);
	property->_class->setListen(property,listen_port);
	property->_class->setListenQ(property,LISTENQ);
	struct TSocks* tsocks = TSocksFactory_class_table.make(property);
	//Run TSocks
	tsocks->_class->run(tsocks);
	//Clean up
	tsocks->_class->destruct(tsocks);
	property = property->_class->destruct(property);
	return 0;
}

#define MAX_BUF_SIZE 1024
unsigned short valid_port(unsigned short port, char* name,unsigned short default_port){
	if(0==port){
		char errormsg[MAX_BUF_SIZE]; bzero(errormsg,sizeof(errormsg));
		sprintf(errormsg,"Invalid %s using default %d",name,default_port);
		perror(errormsg);
		return default_port;
	} else
		return port;
}

unsigned short itous(int v){
	if(v>USHRT_MAX||v<0)
		return 0;
	else 
		return v;
}
