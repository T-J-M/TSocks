#include <Connection.h>
#include <ConnectionListener.h>
#include <ConnectionManager.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <refcount.h>

struct ConnectionListener_class ConnectionListener_class_table = {
	NULL, /* super */
	addConn,
	removeConn,
	sleep,
	destruct
};

void ConnectionListener_ctor(void* thisv){
	struct ConnectionListener* this = thisv;
	bzero(this,sizeof(struct ConnectionListener));
	this->_class = &ConnectionListener_class_table;
	this->epfd = epoll_create(EPOLL_QUEUE_LEN);
}

void addConn(void* thisv, void* connv, uint32_t events){
	struct ConnectionListener* this = thisv;
	struct Connection* conn = connv;
	struct epoll_event epev; bzero(&epev,sizeof(epev));
	epev.events = events;
	int fd = conn->_class->getFD(conn);
	epev.data.fd = fd;
	epoll_ctl(this->epfd,EPOLL_CTL_ADD,fd,&epev);
}

void removeConn(void* thisv, void* connv){
	struct ConnectionListener* this = thisv;
	struct Connection* conn = connv;
	int fd = conn->_class->getFD(conn);
	epoll_ctl(this->epfd,EPOLL_CTL_DEL,fd,NULL);
}

void sleep(void* thisv){
	struct ConnectionListener* this = thisv;
	int nfds = epoll_wait(this->epfd,this->events,EPOLL_QUEUE_LEN,INT_MAX);
	if(-1==nfds){
		perror("Error: failure occured with epoll_wait");
	} else {
		for(int n=0; n<nfds; n++){
			struct epoll_event event = this->events[n];
			int fd = event.data.fd;
			struct ConnectionManager* connman = ConnectionManager_class_table.getInstance();
			struct Connection* conn = connman->_class->getConnByFD(connman,fd);
			struct ConnectionState_class* state = conn->_class->getState(conn);
			state->handle(conn);
		}
	}
}

void* destruct(void* thisv){
	struct ConnectionListener* this = thisv;
	rc_free_ref(this);
	return NULL;
}
