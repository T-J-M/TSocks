#include <ListenConnFactory.h>
#include <refcount.h>
#include <limits.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <assert.h>
#include <linux/netfilter_ipv4.h>
#include <sys/epoll.h>

#define SA struct sockaddr

struct ListenConnFactory_class ListenConnFactory_class_table ={
	&ConnectionFactory_class_table, /* super */
	make
};

struct Connection* make(void* argv){
	int* potential_argv = argv;
	if(*potential_argv>USHRT_MAX){
		//Address, call copy constructor in super class
		struct Connection* conn = ListenConnFactory_class_table.super->make(argv);
		return conn;
	} else {
		//Port, listen and bind on that port
		unsigned short port = (unsigned short) argv;
		struct Connection* conn = ListenConnFactory_class_table.super->make(NULL);
		const int listenfd = socket(AF_INET, SOCK_STREAM,IPPROTO_TCP);

		const int enable_reuseaddr = ENABLE_REUSEADDR;
		if(setsockopt(listenfd,SOL_SOCKET,SO_REUSEADDR,&enable_reuseaddr,sizeof(enable_reuseaddr)))
			perror("Warning: Cannot reuse address");

		struct sockaddr_in servaddr; bzero(&servaddr,sizeof(servaddr));
		servaddr.sin_family = AF_INET;
		servaddr.sin_addr.s_addr = INADDR_ANY;
		servaddr.sin_port = htons(port);
		bind(listenfd,(SA *) &servaddr,sizeof(servaddr));

		//Setup FD and state
		conn->_class->setFD(conn, listenfd);
		conn->_class->setState(&NewConnState_class_table);
		return conn;
	}
}
