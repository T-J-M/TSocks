#include <FDmap.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <refcount.h>

struct FDmap_class FDmap_class_table = {
	NULL, /* super */
	insert,
	query,
	destruct
};

struct node{
	int key;
	void* val;
	struct node* next;
};

#ifndef DEFAULT_TABLE_SIZE
#define DEFAULT_TABLE_SIZE 2333
#endif

void FDmap_ctor(void* thisv){
	struct FDmap* this = thisv;
	bzero(this,sizeof(struct FDmap));
	this->size = DEFAULT_TABLE_SIZE;
	struct node** table = (struct node**) calloc(this->size,sizeof(struct node*));
	this->data = table;
	bzero(this->data,this->size*sizeof(struct node));
}

void* destruct(void* thisv){
	struct FDmap* this = thisv;
	struct node** table = this->data;
	for(int i=0; i<this->size;i++){
		struct node* probe = table[i];
		while(probe!=NULL){
			struct node* next = probe->next;
			free(probe);
			probe = next;
		}
	}
	free(this->data);
	rc_free_ref(this);
	return NULL;
}

int hash(struct FDmap* map, int key){
	if(key<0)
		return -(key%map->size);
	else 
		return key%map->size;
}

void insert(void* thisv, int key, void* val){
	struct FDmap* this = thisv;
	struct node** table = this->data;
	int pos = hash(this,key);
	struct node* bucket = table[pos];
	struct node* probe = bucket;
	while(probe!=NULL){
		if(probe->key==key){
			probe->val = val;
			return;
		}
		probe = probe->next;
	}
	struct node* newNode = malloc(sizeof(struct node));
	bzero(newNode,sizeof(struct node));
	newNode->key = key;
	newNode->val = val;
	newNode->next = bucket;
	table[pos] = newNode;
}

void* query(void* thisv, int key){
	struct FDmap* this = thisv;
	struct node** table = this->data;
	int pos = hash(this,key);
	struct node* bucket = table[pos];
	struct node* probe = bucket;
	while(probe!=NULL){
		if(probe->key==key){
			return probe->val;
		}
		probe = probe->next;
	}
	return NULL;
}
