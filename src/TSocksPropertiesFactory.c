#include <TSocksPropertiesFactory.h>
#include <TSocksProperties.h>
#include <refcount.h>

struct TSocksPropertiesFactory_class TSocksPropertiesFactory_class_table = {
	NULL, /* super */
	make 
};

struct TSocksProperties* make(void* orig){
	struct TSocksProperties* newObj = rc_malloc(sizeof(struct TSocksProperties));
	TSocksProperties_ctor(newObj);
	if(NULL!=orig){
		char * host_p = orig->_class->getHostStr(orig);
		newObj->_class->setHost(newObj,host_p);
		unsigned short port = orig->_class->getPort(orig);
		newObj->_class->setPort(newObj,port);
		unsigned short listen = orig->_class->getListen(orig);
		newObj->_class->setListen(newObj,listen);
		unsigned short listenQ = orig->_class->getListenQ(orig);
		newObj->_class->setListenQ(newObj,listenQ);
	}
	return newObj;
}
