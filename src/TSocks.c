#include <stddef.h>
#include <string.h>
#include <TSocks.h>
#include <TSocksProperties.h>
#include <ConnectionListener.h>
#include <ConnectionManager.h>
#include <BindConnFactory.h>
#include <refcount.h>

struct TSocks_class TSocks_class_table = {
	NULL, /* super */
	run,
	getProperties,
	setProperties,
	destruct
};

void TSocks_ctor(void* thisv){
	struct TSocks* this = thisv;
	bzero(this,sizeof(struct TSocks));
	this->_class = &TSocks_class_table;
}

int run(void* thisv){
	struct TSocks* this = thisv;
	struct TSocksProperties* properties = this->properties;
	//Generate connection listener
	struct ConnectionListener* listener = rc_malloc(sizeof(struct ConnectionListener));
	ConnectionListener_ctor(listener);
	//Connection Manager
	struct ConnectionManager * manager = ConnectionManager_class_table.getInstance();
	//Generate bind connection
	unsigned short bindPort = properties->_class->getListen(properties);
	struct Connection* bind = manager->_class->getNewConn(manager,&BindConnFactory_class_table,&bindPort);
		//Actual listen should perform in this step
	const uint32_t bindListenMask = EPOLLIN|EPOLLRDNORM|EPOLLPRI|EPOLLERR|EPOLLHUP;
	listener->_class->addConn(listener,bind,bindListenMask);
	for(;;){
		//Wait for connection
		//Connection will be handled in ConnectionState
		listener->_class->sleep(listener);
	}

	//De-register bind connection from listener
	listener->_class->removeConn(listener,bind);
	//Cleanup connection listener
	listener->_class->destruct(listener);
	return 0;
}

struct TSocksProperties* getProperties(void* thisv){
	struct TSocks* this = thisv;
	return this->properties;
}

void setProperties (void* thisv, struct TSocksProperties* properties){
	struct TSocks* this = thisv;
	this->properties = properties;
}

void* destruct(void* thisv){
	struct TSocks* this = thisv;
	struct TSocksProperties* properties = this->properties;
	properties->_class->destruct(properties);
	rc_free_ref(this);
}

