#include <stddef.h>
#include <TSocksFactory.h>
#include <TSocksProperties.h>
#include <TSocks.h>
#include <refcount.h>

struct TSocksFactory_class TSocksFactory_class_table = {
	NULL, /* super */
	make 
};

struct TSocks* make(void* propertiesv){
	struct TSocks* newObj = rc_malloc(sizeof(struct TSocks));
	TSocks_ctor(newObj);
	struct TSocksProperties * properties = propertiesv;
	if(NULL!=properties){
		newObj->_class->setProperties(properties);
		rf_keep_ref(properties);
	}
	return newObj;
}
