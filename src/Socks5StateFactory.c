#include <stddef.h>
#include <Socks5StateFactory.h>
#include <Socks5State.h>
#include <Socks5HandShake.h>
#include <Socks5ConnCMD.h>
#include <refcount.h>

struct Socks5StateFactory_class Socks5StateFactory_class_table = {
	NULL, /* super */
	make
};

struct Socks5State* make(void* argv){
	struct sockaddr_in* dst_addr = argv;
	//Command state
	struct Socks5ConnCMD* command = rc_malloc(sizeof(struct Socks5ConnCMD));
	void* args[2] = {NULL,dst_addr};
	Socks5ConnCMD_ctor(command,&args);
	//HandShake state
	struct Socks5HandShake* handshake = rc_malloc(sizeof(struct Socks5HandShake));
	Socks5HandShake_ctor(handshake,command);
	return (struct Socks5State*) handshake;
}
